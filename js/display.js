import {removeFilter} from './main.js';

export function displaySpinner(searchButton) {
    var spinner = document.createElement('span');
    spinner.classList.add('spinner-border');
    spinner.classList.add('spinner-border-sm');
    spinner.setAttribute('id', 'spinner');
    searchButton.textContent = '';
    searchButton.append(spinner);
}

export function deleteSpinner() {
    var searchButton = document.getElementById('searchButton');
    var spinner = document.getElementById('spinner');
    searchButton.textContent = 'search';
    spinner.remove();
}

export function displayFilter(filter) {
    var filterDiv = document.getElementById('filter');
    var newfilter = document.createElement('div');
    newfilter.classList.add('badge');
    newfilter.classList.add('badge-secondary');
    newfilter.classList.add('filter');
    newfilter.textContent = filter;
    filterDiv.append(newfilter);
    filter = document.getElementsByClassName('filter');
    removeEventFilter(filter);
}

function removeEventFilter(filter) {
    Array.from(filter).forEach(element => {
        element.addEventListener("click", function() {
            removeFilter(element.innerHTML);
            element.remove();
        });
    });
}

