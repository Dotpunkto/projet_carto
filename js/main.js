import {getBeerPosition, getPosition} from "./getBrewery.js";
import {displayPos, deleteMarker} from "./map.js";
import {displaySpinner, displayFilter} from "./display.js";

var search = document.getElementById('searchButton');
var input = document.getElementById('input');
var filter1 = document.getElementsByClassName('filterStyle');
var filter2 = document.getElementsByClassName('filterCategorie');
var style = ' ';
var categorie = ' ';
var country = [
    "United States",
    "Belgium",
    "Germany",
    "United Kingdom",
    "Canada",
    "Netherlands",
    "Austria",
    "Switzerland",
    "Australia",
    "Norway",
    "Japan",
    "France",
    "Czech Republic",
    "Ireland",
    "India",
    "Mexico",
    "New Zealand",
    "Poland",
    "Italy",
    "England",
    "Sweden",
    "Brazil",
    "Denmark",
    "Russia",
    "El Salvador",
    "Spain",
    "Argentina",
    "Hungary",
    "Lithuania",
    "Finland",
    "Jamaica",
    "Korea, Republic of",
    "Latvia",
    "Panama",
    "China",
    "Greece",
    "Philippines",
    "Sri Lanka",
    "Thailand",
    "Aruba",
    "Belize",
    "Colombia",
    "Croatia",
    "Cuba",
    "Egypt",
    "Estonia",
    "French Polynesia",
    "Guatemala",
    "Honduras",
    "Israel",
    "Kenya",
    "Macao",
    "Macedonia, the Former Yugoslav Republic of",
    "Mauritius",
    "Myanmar",
    "Namibia",
    "Portugal",
    "Sierra Leone",
    "Slovakia",
    "Taiwan, Province of China",
    "Togo"];

Array.from(filter1).forEach(element => {
    element.addEventListener("click",function() {
        if (style == ' '){
            style = element.innerHTML;
            displayFilter(style);
        }
    });
});

Array.from(filter2).forEach(element => {
    element.addEventListener("click",function() {
        if (categorie == ' '){
            categorie = element.innerHTML;
            displayFilter(categorie);
        }
    });
});

search.onclick = function() {
    search.disabled = true;
    displaySpinner(search);
    deleteMarker();
    var putFirstLetterIntoMaj = strUcFirst(input.value);
    inputFilter(putFirstLetterIntoMaj);
}

input.addEventListener("keyup", function(event) {
    if (event.keyCode === 13 && search.disabled != true) {
        search.disabled = true;
        displaySpinner(search);
        deleteMarker();
        var putFirstLetterIntoMaj = strUcFirst(input.value);
        inputFilter(putFirstLetterIntoMaj);
    }
});

// transform first letter into uppercase.
function strUcFirst(input) {
    return (input+'').charAt(0).toUpperCase()+input.substr(1);
}

function inputFilter(value) {
    if(value == '') {
        getBeerPosition(value, 5973, style, categorie, [0,0]);
    } else if(country.includes(value)) {
        getBeerPosition(value, 4588, style, categorie, [0,0]);
    } else {
        getPosition(getBeer, value);
    }
}

function getBeer(pos) {
    displayPos(pos);
    getBeerPosition(' ', 20, style, categorie, pos);
}

export function removeFilter(filter) {
    if(filter == style) {
        style = ' ';
    } else if(filter == categorie) {
        categorie = ' ';
    }
}