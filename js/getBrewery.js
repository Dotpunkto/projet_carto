import {manageDataBeer, manageDataPosition} from "./manageData.js";


export function getBeerPosition(country, rows, style, categorie, pos) {
    const url = new URL('https://data.opendatasoft.com/api/records/1.0/search/?dataset=open-beer-database%40public-us&');
    var param = paramBeerData(country, rows, style, categorie, pos);

    console.log(url+param);
    fetch(url+param)
        .then(
        function(response) {
            if (response.status !== 200) {
                console.log('Looks like there was a problem. Status Code: ' + response.status);
                return;
            }
        
            response.json().then(function(dataBeer) {
                manageDataBeer(dataBeer);
            });
        }
    ).catch(function(err) {
        console.log('Fetch Error :-S', err);
    });
}

function paramBeerData(country, rows, style, categorie, pos) {
    const params = new URLSearchParams();
    if(style != ' ') {
        params.set('refine.style_name', style);
    }
    if(categorie != ' ') {
        params.set('refine.cat_name', categorie);
    }
    if(country != ' ' && country != '') {
        params.set('refine.country', country);
    }
    if(pos[0] != 0 && pos[1] != 0){
        params.set('geofilter.distance', pos[0]+','+pos[1]+',500000');
    }
    if(rows != 0) {
        params.set('rows', rows);
    }
    return params.toString();
}

export function getPosition(callback, address) {
    const url = new URL('https://eu1.locationiq.com/v1/search.php?');
    var param = paramPositionData(address);

    console.log(url+param);
    fetch(url+param)
        .then(
        function(response) {
            if (response.status !== 200) {
            console.log('Looks like there was a problem. Status Code: ' + response.status);
            return;
            }

            response.json().then(function(dataPosition) {
                callback(manageDataPosition(dataPosition));
            });
        }
    ).catch(function(err) {
        console.log('Fetch Error :-S', err);
    });
}

function paramPositionData(address) {
    const params = new URLSearchParams();
    params.set('key', '14fb7324a12156');
    params.set('q', address);
    params.set('format', 'json');
    return params.toString();
}