import {deleteSpinner} from './display.js';

var mymap = L.map('map').setView([50.850340, 4.351710],3);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(mymap);

var marker;
var markersCluster;
var myIcon;
var iconPos;

function getMarkersCluster() {
    return new L.MarkerClusterGroup({
        iconCreateFunction: function(cluster) {
            return L.divIcon({ 
                html: cluster.getChildCount(), 
                className: 'mycluster', 
                iconSize: null 
            });
        }
    });
}

function getMyIcon() {
    return L.icon({
        iconUrl: './img/beer_2.png',
        iconSize: [68, 68],
        iconAnchor: [32, 80],
        popupAnchor: [-3, -76]
    });
}

function getIconPos() {
    return L.icon({
        iconUrl: './img/pin.png',
        iconSize: [50, 50],
        shadowSize: [68, 95],
        shadowAnchor: [22, 94]
    });
}

export function pointsMap(arrayInformations) {
    markersCluster = getMarkersCluster();
    myIcon = getMyIcon();
    for(var i=0;i<arrayInformations.length;i++) {
        var latLng = new L.LatLng(arrayInformations[i]["coordinatesLat"], arrayInformations[i]["coordinatesLon"]);
        var beerMarker = new L.Marker(latLng, {icon: myIcon});
        var popup = new L.popup().setLatLng(latLng)
        .setContent(arrayInformations[i]["nameKey"]+'<br/>Address: '+arrayInformations[i]["address"]+
        '<br/>Country: '+arrayInformations[i]["countrykey"]+'<br/>Brewerie: '+arrayInformations[i]["brewerie"]).openPopup();
        beerMarker.bindPopup(popup);
        markersCluster.addLayer(beerMarker);
    }
    mymap.addLayer(markersCluster);
    enableSearchButton();
    deleteSpinner();
}

function enableSearchButton() {
    var button = document.getElementById('searchButton');
    button.disabled = false;
}



export function deleteMarker() {
    if(marker) {
        console.log("removing marker", marker);
        marker.removeFrom(mymap);
    }
    if(markersCluster){
        markersCluster.remove();
    }
}

export function displayPos(pos) {
    iconPos = getIconPos();
    mymap.setView([pos[0], pos[1]], 12);
    marker = L.marker([pos[0], pos[1]], {icon: iconPos}).addTo(mymap);
    marker.bindPopup("you are here !!!").openPopup();
}
