import {pointsMap} from "./map.js";

export function manageDataBeer(dataBeer) {
    var arrayInformations=[];
    for(let i=0; i<dataBeer.records.length; i++) {
        let Informations;
        let coordinatesLat;
        let coordinatesLon;
        let name = isEmpty(dataBeer.records[i].fields.name);
        let country = isEmpty(dataBeer.records[i].fields.country);
        let address = isEmpty(dataBeer.records[i].fields.address1);
        let brewerie = isEmpty(dataBeer.records[i].fields.name_breweries);
        
        if(dataBeer.records[i].fields.coordinates) {
            coordinatesLat = isEmpty(dataBeer.records[i].fields.coordinates[0]);
            coordinatesLon = isEmpty(dataBeer.records[i].fields.coordinates[1]);
            Informations = {"nameKey": name, "countrykey": country, "coordinatesLat": coordinatesLat, 
                "coordinatesLon": coordinatesLon, "address": address, "brewerie": brewerie};
                arrayInformations.push(Informations);
        }
    }
    pointsMap(arrayInformations)
}

export function manageDataPosition(dataPosition) {
    var position = [];
    position[0] = dataPosition[0].lat;
    position[1] = dataPosition[0].lon;
    return position;
}

function isEmpty(test) {
    if(!test) {
        return 'N/A';
    }
    return test;
}