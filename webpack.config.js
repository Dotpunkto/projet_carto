const path = require("path");

module.exports = {
	mode: 'development',
	entry: './js/index.js',
	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: 'my-first-webpack.bundle.js',
		sourceMapFilename: "my-first-webpack.bundle.js.map"
	},
	module: {
		rules: [
		{
			test: /\.css$/,
			use: ['style-loader', 'css-loader']
		}
	]
	},
	devtool: 'source-map'
}